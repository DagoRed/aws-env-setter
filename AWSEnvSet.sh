#!/usr/bin/bash

export ENVIRONMENT_NAME="ExampleDev"
export ENV="DEV" # Example of an additional variable 

export AWS_ACCESS_KEY_ID=MY_AWS_ACCESS_ID_HERE
export AWS_SECRET_ACCESS_KEY=MY_AWS_SECRET_KEY_HERE

# export AWS_ROLE_ARN=ROLE_I_WISH_TO_IMPERSONATE_HERE_WITH_TRUST_SHARE
# export AWS_ROLE_SESSION_NAME=NAME_I_WANT_TO_KNOW_NO_SPACES

export LOG_PATH=../../logs

deactivate () {
    # This should detect bash and zsh, which have a hash command that must
    # be called to get it to forget past commands.  Without forgetting
    # past commands the $PATH changes we made may not be respected
    if [ -n "${BASH-}" ] || [ -n "${ZSH_VERSION-}" ] ; then
        hash -r 2>/dev/null
    fi

    if ! [ -z "${_OLD_VIRTUAL_PS1+_}" ] ; then
        PS1="$_OLD_VIRTUAL_PS1"
        export PS1
        unset _OLD_VIRTUAL_PS1
    fi

    if [ ! "${1-}" = "nondestructive" ] ; then
    # Self destruct!
        unset -f deactivate
        unset AWS_ACCESS_KEY_ID
        unset AWS_SECRET_ACCESS_KEY
        unset ENV   # Example of unsetting a custom variable
        unset AWS_ROLE_ARN
        unset AWS_ROLE_SESSION_NAME
        unset AWS_SECURITY_TOKEN
    fi
}

impersonate () {
    # Used to impersonate a user. 

    credentials=$(aws sts assume-role --role-arn $1 --role-session-name $2 --query 'Credentials.{AKI:AccessKeyId,SAK:SecretAccessKey,ST:SessionToken}' --output text)

    export AWS_ACCESS_KEY_ID=$(echo $credentials | awk '{print $1}')
    export AWS_SECRET_ACCESS_KEY=$(echo $credentials | awk '{print $2}')
    export AWS_SECURITY_TOKEN=$(echo $credentials | awk '{print $3}')
}

testawsid () {
    # Debug with this:
    aws sts get-caller-identity
}

# unset irrelevant variables
deactivate nondestructive


if [ -z "${AWS_DEPLOYER}" ] ; then
    _OLD_VIRTUAL_PS1="$PS1"
    if [ "x" != x ] ; then
        PS1="$PS1"
    else 
        PS1="(\e[1;34m${ENVIRONMENT_NAME}\e[0;37m) $PS1\n\e[1;34m>\e[1;32m$\e[0;37m "
        # \e[1;XXm will change the color for the text after it. The bash colors are
        # pretty standard. 
        # Recommend green for dev, yellow for qa/staging, red for production. 

        # impersonate $AWS_ROLE_ARN $AWS_ROLE_SESSION_NAME
    fi
    export PS1
fi

