[[_TOC_]]

# AWS Environment Setter

Script is written to support setting an environment via shell script and to impersonate, and test which credentials are
being use. An impersonate function is available to impersonate other accounts. 

# Installation
Post the script where you want and due to the nature of having keys in the script, set permission to `chmod 700`. 
To use, simply call `source ~/path/to/script/awsMyEnvdev.sh`. 

For each evironment, set the the Environment name and color. 

**Warning:** Use with git bash is a bit strange. 

# Stop Usage

Simply use the `deactivate` command and the shell line will return to normal.

